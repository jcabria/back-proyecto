var express = require('express');
var app     = express();
var bodyParser = require ('body-parser');
app.use(bodyParser.json()); //

var baseMLabURL="https://api.mlab.com/api/1/databases/apitechujcptu/collections/";
var mLabAPIKey = "apiKey=JcZrqbnC104WGuBrlZKG4nZukV4BxtqL";
var requestJson = require('request-json');

var port    = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get ("/apitechu/v1",
  function(req, res) {
   console.log("GET /apitechu/v1");
   res.send({"msg" : "Hola desde APITechU"});
  }
);

app.get("/apitechu/v1/users",
  function(req, res) {
      console.log("GET /apitechu/v1/users");

      // res.sendFile('./usuarios.json'); // deprecated
      res.sendFile('usuarios.json', {root: __dirname});

      // var users = require('./usuarios.json');
      // res.send(users);
  }
);

app.post("/apitechu/v1/users",
  function(req, res){
    console.log("POST /apitechu/v1/users");
    /* se puede hacer el post en el header
    console.log(req.headers);
    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.country);

    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    };
    */

    // o se puede hacer el post en el body
    console.log("first_name is " + req.body.first_name);
    console.log("last_name is " + req.body.last_name);
    console.log("country is " + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    // metiendo usuarios en una variable
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);
    var msg = "Usuario guardado con éxito";
    console.log(msg);
    res.send({"msg" : msg});
  }
)

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);

    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
  }
)

// función de escritura de datos en el fichero
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
        console.log(err);
      } else{
        console.log("Datos escritos en archivo");
      }
    }
  )
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
)

app.get("/apitechu/v1/login",
  function(req, res) {
      console.log("GET /apitechu/v1/login");
      res.sendFile('usuarios.json', {root: __dirname});
  }
);

app.post("/apitechu/v1/login",
  function(req, res){
    console.log("POST /apitechu/v1/login");

    var user = {
      "email" : req.body.email,
      "pwd" : req.body.pwd
    };

    //cargo la lista de cuentasEmail.json
    var totalUsers = require('./usuarios.json');

    for (userDeFichero of totalUsers){
      if ((user.email == userDeFichero.email) && (user.pwd == userDeFichero.pwd)) {  // verifica la cuenta de email y pwd
          userDeFichero.logged = true;
          writeUserDataToFile(totalUsers);
          res.send({"mensaje" : "Login correcto", "idUsuario" : userDeFichero.id});
        }
      }
      //cuenta de email o pwd incorrectas
      res.send({"mensaje" : "Login incorrecto"});
    }
);

// -----------------------------------------------------//

app.post("/apitechu/v1/logout",
  function(req, res){
    console.log("POST /apitechu/v1/logout");

    var user = {
      "id" : req.body.id
    };

    //cargo la lista de cuentasEmail.json
    var totalUsers = require('./usuarios.json');

    for (userDeFichero of totalUsers){
      if ((user.id == userDeFichero.id) && (userDeFichero.logged == true)) {  // verifica el id y que esté logado
          delete userDeFichero.logged;
          writeUserDataToFile(totalUsers);
          res.send({"mensaje" : "Logout correcto", "idUsuario" : userDeFichero.id});
      }
    }
    res.send({"mensaje" : "Logout incorrecto"});
  }
);



app.get("/apitechu/v2/users",
  function(req, res) {
      console.log("GET /apitechu/v2/users");

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP Creado");
      httpClient.get ("user?" + mLabAPIKey, function(err, resMlab, body) {
        var response = !err ? body : {
          "msg" : "Errpr obteniendo usuarios"
        }
        res.send(response);
      }
    );
  }
);



app.get("/apitechu/v2/users/:id",
  function(req, res) {
      console.log("GET /apitechu/v2/users/:id");

      var id = req.params.id;
      var query = 'q={"id":' + id + '}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP Creado");
      httpClient.get ("user?" + query + "&" + mLabAPIKey,
      function(err, resMlab, body) {
      //  var response = !err ? body : {
        //  "msg" : "Errpr obteniendo usuario."
      //  }

      var response = {};
      if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
         }
         res.status(500)
      } else {
        if (body.length > 0) {
          response=body;
        } else {
          response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }
        res.send(response);
      }
    );
  }
);



////////////////////////// LOGIN ///////////////////////

app.post("/apitechu/v2/login",
 function(req, res){
   console.log("POST /apitechu/v2/login");

   var query = 'q={"email":"' + req.body.email + '", "pwd":"' + req.body.pwd + '"}';
   console.log(query);

   var httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente HTTP creado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       var response = {};
       console.log(body);

       if (err) { // si no encuentra un registro
         response = {
           "msg" : "Usuario o pwd no encontrados"
         }
       } else { // si encuentra un registro
         var query = 'q={"id" : ' + body[0].id +'}'; // cojo el id del registro que coincide
         var putBody = '{"$set":{"logged":true}}';

         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err2,resMLab2, body2){
             // controlamos el resultado de la query
             if (err) {
               console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
               console.log(resMLab);
               response = {
                 "msg" : "Error al hacer login"
               }
               res.status(500);
             } else {
               if (body.length > 0){
                 response = body;

               } else {
                 response = {
                   "msg" : "Usuario no encontrado"
                 };
                 res.status(404);
               }
             }
             console.log("la id del usuario es: " + response[0].id);
             res.send(response[0]);
           }
         );
       }
     }
   );
 }
);


////////////////////////// LOGOUT ///////////////////////

app.post("/apitechu/v2/logout",
 function(req, res){
   console.log("POST /apitechu/v2/logout");

   var query = 'q={"id":"' + req.body.id + '"}';
   console.log(query);

   var httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente HTTP creado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       var response = {};
       console.log(body);

       if (err) { // si no encuentra un registro
         response = {
           "msg" : "Usuario o pwd no encontrados"
         }
       } else { // si encuentra un registro
         var query = 'q={"id" : ' + req.body.id +'}'; // cojo el id del registro que coincide
         var putBody = '{"$set":{"logged":false}}';

         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err2,resMLab2, body2){
             // controlamos el resultado de la query
             if (err) {
               console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
               console.log(resMLab);
               response = {
                 "msg" : "Error al hacer logout"
               }
               res.status(500);
             } else {
               if (body2.length > 0){
                 response = body;

               } else {
                 response = {
                   "msg" : "Usuario no encontrado"
                 };
                 res.status(404);
               }
             }
             console.log("la id del usuario es: " + req.body.id);
             res.send(response);
           }
         );
       }
     }
   );
 }
);


////////////////////////// LOGOUT ///////////////////////


/*
app.post("/apitechu/v2/logout",
 function(req, res){
   console.log("POST /apitechu/v2/logout");

   var query = 'q={"email":"' + req.body.email + '", "pwd":"' + req.body.pwd + '"}';
   console.log(query);

   var httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente HTTP creado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       var response = {};
       console.log(body);

       if (err) { // si no encuentra un registro
         response = {
           "msg" : "Usuario o pwd no encontrados"
         }
       } else { // si encuentra un registro
         var query = 'q={"id" : ' + body[0].id +'}'; // cojo el id del registro que coincide
         var putBody = '{"$set":{"logged":true}}';

         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(err2,resMLab2, body2){
             // controlamos el resultado de la query
             if (err) {
               console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
               console.log(resMLab);
               response = {
                 "msg" : "Error al hacer logout"
               }
               res.status(500);
             } else {
               if (body.length > 0){
                 response = body;

               } else {
                 response = {
                   "msg" : "Usuario no encontrado"
                 };
                 res.status(404);
               }
             }
             console.log("la id del usuario es: " + response[0].id);
             res.send(response[0]);
           }
         );
       }
     }
   );
 }
);


*/









///////////////// GET DE CUENTAS /////////////////

app.get("/apitechu/v2/accounts",
  function(req, res) {
      console.log("GET /apitechu/v2/accounts");

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP Creado");
      httpClient.get ("account?" + mLabAPIKey, function(err, resMlab, body) {
        var response = !err ? body : {
          "msg" : "Errpr obteniendo usuarios"
        }
        res.send(response);
      }
    );
  }
);


app.get("/apitechu/v2/users/:id/accounts",
  function(req, res) {
      console.log("GET /apitechu/v2/users/:id/accounts");

      var user_id = req.params.id;
      var query = 'q={"user_id":' + user_id + '}';
      console.log(user_id);
      console.log(query);

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP Creado");
      httpClient.get ("account?" +  query +  "&" + mLabAPIKey,
      function(err, resMlab, body) {

      var response = {};
      if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
         }
         res.status(500)
      } else {
        if (body.length > 0) {
          response=body;
        } else {
          response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }
        res.send(response);
      }
    );
  }
);




///////////////// GET DE LOS MOVIMIENTOS /////////////////


app.get("/apitechu/v2/movements",
  function(req, res) {
      console.log("GET /apitechu/v2/movements");

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP Creado");
      httpClient.get ("movements?" + mLabAPIKey, function(err, resMlab, body) {
        var response = !err ? body : {
          "msg" : "Errpr obteniendo usuarios"
        }
        res.send(response);
      }
    );
  }
);
//"user_id":' + user_id + ',

app.get("/apitechu/v2/users/:id/:idCuenta",
  function(req, res) {
      console.log("GET /apitechu/v2/users/:id/:idCuenta");

      var idCuenta = req.params.idCuenta;
      var IBAN = req.params.IBAN;
      var query = 'q={"idCuenta":' + idCuenta +'}';
      console.log(idCuenta);
      console.log(IBAN);
      console.log(query);

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP Creado");
      httpClient.get ("movements?" +  query +  "&" + mLabAPIKey,
      function(err, resMlab, body) {

      var response = {};
      if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
         }
         res.status(500)
      } else {
        if (body.length > 0) {
          response=body;
        } else {
          response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }
        res.send(response);
      }
    );
  }
);



////////////////////////// ALTA Usuarios ///////////////////////

app.post("/apitechu/v2/alta",
 function(req, res){
   console.log("POST /apitechu/v2/alta");

   console.log(query);

   var httpClient = requestJson.createClient(baseMLabURL);
   var querySort = 's={"id":-1}';
   var query
   var id
   console.log("Cliente HTTP creado");

   httpClient.get("user?" + querySort + "&" + mLabAPIKey,
     function(err, resMLab, body){
       var response = {};
       id = parseInt(body[0].id) + parseInt(1);
       if (err) { // si no encuentra un registro
         response = {
           "msg" : "Usuario o pwd no encontrados"
         }
       } else { // si encuentra un registro
         query = '{"id":'+ id + ', "first_name":"' + req.body.nombre + '", "last_name":"' + req.body.apellido + '" , "email":"' + req.body.email + '", "pwd":"' + req.body.pwd + '"}';

            httpClient.post("user?" + query + "&" + mLabAPIKey, JSON.parse(query),
                   function(err2,resMLab2, body2){
                     // controlamos el resultado de la query
                     if (err2) {
                       console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
                       console.log(resMLab2);
                       response = {
                         "msg" : "Error al hacer login"
                       }
                       res.status(500);
                     } else {
                       if (body2.length > 0){
                         response = body2;

                       } else {
                         response = {
                           "msg" : "Usuario no encontrado"
                         };
                         res.status(404);
                       }
                     }
                     console.log("la id del usuario es: " + response[0]);
                     var querySortC = 's={"idCuenta":-1}';
                     var queryC
                     var idC
                     var userC = 5
                     console.log("Cliente HTTP creado");

                     httpClient.get("account?" + querySortC + "&" + mLabAPIKey,
                       function(err, resMLab, body){
                         var response = {};
                         idC = parseInt(body[0].idCuenta) + parseInt(1);
                         var ibanC = parseInt(100000000000) + idC;
                         console.log("ID NUEVA NUEVO:" + idC);
                         if (err) { // si no encuentra un registro
                           response = {
                             "msg" : "Cuenta no encontrada"
                                           }
                         } else { // si encuentra un registro
                           queryC = '{"idCuenta":'+ idC + ', "IBAN":"AD24 8462 7020 ' + ibanC + '", "user_id":' + id + ' , "balance":"0"}';

                              httpClient.post("account?" + queryC + "&" + mLabAPIKey, JSON.parse(queryC),
                                     function(err2,resMLab2, body2){
                                       // controlamos el resultado de la query
                                       if (err2) {
                                         console.log(baseMLabURL + "account?" + queryC + "&" + mLabAPIKey);
                                         console.log(resMLab2);
                                         response = {
                                           "msg" : "Error al hacer login"
                                         }
                                         res.status(500);
                                       } else {
                                         if (body2.length > 0){
                                           response = body2;

                                         } else {
                                           response = {
                                             "msg" : "Cuenta agregada"
                                           };
                                           res.status(404);
                                         }
                                       }
                                       res.send(response);
                                     }
                                   );
                                 }
                               }
                             );
                   }
                 );
               }
             }
           );
         }
        );


        /*
////////////////////////// ALTA ///////////////////////

app.post("/apitechu/v2/alta",
 function(req, res){
   console.log("POST /apitechu/v2/alta");

   console.log(query);

   var httpClient = requestJson.createClient(baseMLabURL);
   var querySort = 's={"id":-1}';
   var query
   var id
   console.log("Cliente HTTP creado");

   httpClient.get("user?" + querySort + "&" + mLabAPIKey,
     function(err, resMLab, body){
       var response = {};
       id = parseInt(body[0].id) + parseInt(1);
       if (err) { // si no encuentra un registro
         response = {
           "msg" : "Usuario o pwd no encontrados"
         }
       } else { // si encuentra un registro
         query = '{"id":'+ id + ', "first_name":"' + req.body.nombre + '", "last_name":"' + req.body.apellido + '" , "email":"' + req.body.email + '", "pwd":"' + req.body.pwd + '"}';

            httpClient.post("user?" + query + "&" + mLabAPIKey, JSON.parse(query),
                   function(err2,resMLab2, body2){
                     // controlamos el resultado de la query
                     if (err2) {
                       console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
                       console.log(resMLab2);
                       response = {
                         "msg" : "Error al hacer login"
                       }
                       res.status(500);
                     } else {
                       if (body2.length > 0){
                         response = body2;

                       } else {
                         response = {
                           "msg" : "Usuario no encontrado"
                         };
                         res.status(404);
                       }
                     }
                     console.log("la id del usuario es: " + response[0]);
                     res.send(response);
                   }
                 );
               }
             }
           );
         }
    );


*/

    ////////////////////////// ALTA Movimientos ///////////////////////

  app.post("/apitechu/v2/altamov",
   function(req, res){
     console.log("POST /apitechu/v2/altamov");
  var balance =0;
     var httpClient = requestJson.createClient(baseMLabURL);
     console.log("Cliente HTTP creado");

   // si encuentra un registro
           var query = '{ "idCuenta":'+ req.body.id + ', "IBAN":"' + req.body.iban + '", "amount":' + req.body.amount + '}';
           console.log(query);
              httpClient.post("movements?" + query + "&" + mLabAPIKey, JSON.parse(query),
                     function(err2,resMLab2, body2){
                       // controlamos el resultado de la query
                       if (err2) {
                         console.log(baseMLabURL + "movements?" + query + "&" + mLabAPIKey);
                         console.log(resMLab2);
                         response = {
                           "msg" : "Error al insertar movimiento"
                         }
                         res.status(500);
                       } else {
                         if (body2.length > 0){
                           response = body2;

                         } else {
                           response = {
                             "msg" : "Movimiento no encontrado"
                           };
                           res.status(404);
                         }
                       }
                       console.log("el id del movimiento es: " + response);

                     }

             );

             var query3 = 'q={ "idCuenta":' + req.body.id + '}';

             httpClient.get ("account?" +  query3 +  "&" + mLabAPIKey,
             function(err, resMlab, body) {
             var response = {};
             if (err) {
                 response = {
                   "msg" : "Error obteniendo balance."

                }
                res.status(500)
                res.send(response);
             } else {
               if (body.length > 0) {
                 response=body;

                 balance=parseInt(body[0].balance) + parseInt(req.body.amount);
                 console.log("BALANCE " +balance);
               } else {
                 response = {
                   "msg" : "Balance no encontrado"
                 };
                 res.status(404);
                 res.send(response);
               }
             }
             console.log(balance)
             console.log("NEW BALANCE " + balance);
             var query = 'q={"idCuenta" : ' + req.body.id +'}'; // cojo el id del registro que coincide
             var putBody = '{"$set":{"balance": '+ parseInt(balance) +' }}';
             console.log(query);
             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(err,resMLab, body){
                 // controlamos el resultado de la query
                 if (err) {
                   console.log(baseMLabURL + "account?" + query + "&" + mLabAPIKey);
                   console.log(resMLab);
                   response = {
                     "msg" : "Error al setear balance"
                   }
                   res.status(500);
                 } else {
                   if (body.length > 0){
                     response = body;

                   } else {
                     response = {
                       "msg" : "Cuenta no encontrada"
                     };
                     res.status(404);
                   }
                 }
                 console.log("El nuevo balance es" + balance);
                 res.send(response);
               }
             );

             }
           );



           }
          );




//////////////////////////////////// DATOS API AEMET /////////////////////////////

//en pruebas


app.get("https://opendata.aemet.es/opendata/api/valores/climatologicos/inventarioestaciones/todasestaciones/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqYXZpY2FicmlhNjRAaG90bWFpbC5jb20iLCJqdGkiOiIzNjFlYmRhMC0xODFjLTQ4MTgtYmZmNi02OTlmZDBjZTVjZmYiLCJpc3MiOiJBRU1FVCIsImlhdCI6MTUyODE5NDg5MywidXNlcklkIjoiMzYxZWJkYTAtMTgxYy00ODE4LWJmZjYtNjk5ZmQwY2U1Y2ZmIiwicm9sZSI6IiJ9.29D_sgjl8LWkI9O7RJ4_a07svXErojOLMlzU8vCLqFU",
  function(req, res) {
      console.log("GET CLIMA");

        var response = res
        res.send(response);
      }
    );


// CAMBIO DIVISA //


app.get("https://free.currencyconverterapi.com/api/v5/convert?q=USD_EUR&compact=ultra",
  function(req, res) {
      console.log("GET CAMBIO DIVISA");
      res.send(response);
}
);
